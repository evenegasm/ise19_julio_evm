#include "globales.h"

RawSerial serial(USBTX, USBRX, BAUD_RATE);

//Hilos
Thread id_th_rx(osPriorityNormal,1024); 				//hilo receptor
Thread id_th_tx(osPriorityNormal,1024); 				//hilo transmisor
Thread id_th_sistema(osPriorityNormal,1024);  	//hilo de control de tramas
Thread id_th_time(osPriorityRealtime,1024); 			//hilo de tiempo
Thread id_th_muestreo(osPriorityNormal,1024); 	//hilo funcionalidad


int fs = 1; // frecuencia de muestreo
int cadencia = 1; // periodo de almacenamiento de datos

//Estructura de tiempo
time_t seconds;

char bcc;

Queue<char, 50> colaBuffer;
Queue<char, 50> colaTx;

Mutex cerrojo;

int toSeconds(char dia1, char dia0, char mes1, char mes0, char ano1, char ano0, char horas1, char horas0, char min1, char min0, char seg1, char seg0){
	return ((31104000*(10*((int)ano1-48)+(int)ano0-48))+(2592000*(10*((int)mes1-48)+(int)mes0-48))+(86400*(10*((int)dia1-48)+(int)dia0-48))+(3600*(10*((int)horas1-48)+(int)horas0-48))+(60*(10*((int)min1-48)+(int)min0-48))+(10*((int)seg1-48)+(int)seg0-48));
}
