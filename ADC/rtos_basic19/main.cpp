/* 
    Read analog channel from MCP3201 with SPI interface and send it to the serial interface.
*/
 
#include "mbed.h"

#define ADC_SCALE_VALUE 3.3/4096 
 
 
/*
    Header for MCP3201 Analog SAR Converter with SPI interface 
    Version: 1.0
    Julio S.
*/
 
// SPI clock freq. in HZ
// Limits from MCP3201 datasheet are:
// 1500000Hz when vdd=5V
// 800000Hz when vdd=2.7V
// 1MHz should work and is the default value.
#define MCP3201_CLK_FREQ    1000000
 
// MCP3201 SPI channel pinout
// p5 not wired, there are no commands to send for MCP3201 device
SPI MCP3201(p5,p6,p7);
// take control from CS pin to avoid transition changes between write commands
DigitalOut cs(p21); 

Serial pc(USBTX, USBRX); // tx, rx
 Thread t;
// Read the analog value from MCP3201.
// return 12bit interger (0...4095)
int ReadAnalogMCP3201(void)
{
 //uncommente below line only if the MCP3201_CLK_FREQ is diferrent of 1MHz
 //MCP3201.frequency(MCP3201_CLK_FREQ);
 
 cs=0;
 int high_byte = MCP3201.write(0x00);
 int low_byte = MCP3201.write(0x00);
 cs=1;
 int conv_result = ((high_byte & 0x1f) << 7) | ((low_byte >> 1) & 0x7f);  
 
 return conv_result;
}
 #define EVENTO 0x1
void muestrear(){
	t.signal_set(EVENTO);
}


void func_t(){
	pc.printf("Analog read test from MCP3201.\n");
    Ticker tic;
		tic. attach(&muestrear,0.5);
		float r;
	while(1){ 
			Thread::signal_wait (EVENTO);			
			r=(float)ReadAnalogMCP3201() * ADC_SCALE_VALUE; // show value in volts.
			pc.printf("AD channel value: %4.6f Volts.\r\n", r);
			pc.printf("Teorica: %4.6f Hz.\r\n", (r *43000)/(2.09*100000*6800*0.00000001));
			pc.printf("Real value: %4.6f Hz.\r\n", (r *41853)/(2.09*99983*6775*0.00000001));
		
	}
}
int main() 
{
	
	t.start(func_t);
	Thread::wait(osWaitForever);
		
    //pc.printf(" Press any key to get new sample...\r\n\r\n");
    //char c = pc.getc();
		//Thread::wait(1);
}