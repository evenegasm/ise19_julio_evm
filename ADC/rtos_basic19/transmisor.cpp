#include "transmisor.h"
#include "globales.h"

char *CE;
int metadata;

void isr_tx(){
	if(metadata!=4){ //Se envian los caracteres de la trama
		CE = (char*)colaTx.get(0).value.p;
		if(*CE != NULL){
			serial.putc(*CE);
		}else{
			switch(metadata){
				case 0: //Se envia el ETX
					serial.putc((char)ETX);
					metadata++;
				break;
				case 1: //Se hace un salto de linea
					serial.putc((char)0x0A);
					metadata++;
				break;
				case 2:  // Se hace un retorno de carro
					serial.putc((char)0x0D);
					metadata++;
				break;
				case 3: //Se envia el BCC
					serial.putc(bcc);
					metadata++;
				break;
				default:
				break;
			}
		}
	}
}



void funct_th_tx(void){
	osEvent evento;
	serial.attach(isr_tx,Serial::TxIrq);
	while(true){
		evento=id_th_tx.signal_wait(0); //Se espera una se�al para iniciar un transmisi�n
		switch (evento.value.signals) {
			case EVENTO_TX_ACK:		
				metadata=4; //No se envian metadatos
				serial.putc(ACK);
			break;
			case EVENTO_TX_NACK:
				metadata=4;	//No se envian metadatos
				serial.putc(NACK);
			break;
			case EVENTO_TX_TRAMA:
				metadata = 0;
				serial.putc((char)STX);
			break;
		}
	}
}

