/*#include "mbed.h"
 
I2C i2c(p28, p27);  // sda, scl
 
int main()
{
    //wp = 0; // disable write protect
    printf("Writing bytes 0-16\n");
 
    char data[3];
    for(int i=0; i<16; i++) {
        data[0] = 0;     // MSB address
        data[1] = i;     // LSB address
        data[2] = i * 3; // data
        if(i2c.write(0xA0, data, 3)) {
            error("Write failed\n");
        }
        while(i2c.write(0xA0, NULL, 0)); // wait to complete
    }
 
    data[0] = 0;       // MSB address
    data[1] = 255;     // LSB address
    data[2] = 'A';     // data
    if(i2c.write(0xA0, data, 3)) {
        error("Write failed\n");
    }
    while(i2c.write(0xA0, NULL, 0)); // wait to complete
 
    printf("Setting read pointer to 0\n");
 
    data[0] = 0;                   // MSB address
    data[1] = 0;                   // LSB address
    if(i2c.write(0xA0, data, 2)) { // send address, but no data
        error("Write failed\n");
    }
 
    printf("Reading back data bytes 0-16\n");
    
    char response[1];
    for(int i=0; i<256; i++) {
        if(i2c.read(0xA0, response, 1)) {
            error("Read failed\n");
        }
        printf("address %03d = 0x%02X\n", i, response[0]);
    }
 
}
*/
#include "mbed.h"

#define SDA 9
#define SCL 10

I2C i2c(p9, p10);
Serial pc(USBTX, USBRX);
int address = 0xA0;
int pointerAdddress = 0;
char s[64];
 
// function declaration
void writeEEPROM(int address, unsigned int eeaddress, char *data, int size);
void readEEPROM(int address, unsigned int eeaddress, char *data, int size);
 
// this function has 63 bytes write limit
void writeEEPROM(int address, unsigned int eeaddress, char *data, int size)
{
    char i2cBuffer[size + 2];
    i2cBuffer[0] = (unsigned char)(eeaddress >> 8); // MSB
    i2cBuffer[1] = (unsigned char)(eeaddress & 0xFF); // LSB
 
    for (int i = 0; i < size; i++) {
        i2cBuffer[i + 2] = data[i];
    }
 
    int result = i2c.write(address, i2cBuffer, size + 2, false);
    wait_ms(6);
}
 
// this function has no read limit
void readEEPROM(int address, unsigned int eeaddress, char *data, int size)
{
    char i2cBuffer[2];
    i2cBuffer[0] = (unsigned char)(eeaddress >> 8); // MSB
    i2cBuffer[1] = (unsigned char)(eeaddress & 0xFF); // LSB
 
    // Reset eeprom pointer address
    int result = i2c.write(address, i2cBuffer, 2, false);
    wait_ms(6);
 
    // Read eeprom
    i2c.read(address, data, size);
    wait_ms(6);
}
 
int main()
{
		int add =0;
    char data_read[64];
	for (int i =0; i< 10; i++){
    readEEPROM(address, i*64, data_read, 64);
    pc.printf("Previous data stored: %s\n", data_read);
		Thread::wait(250);
		}
    while(1){
        
    pc.gets(s,64);
    char writeDataLen = 0;
    do {writeDataLen++;} while (s[writeDataLen]); // calculate the text length

    writeEEPROM(address, pointerAdddress+add*64, s, writeDataLen);

    pc.printf("Data written: %s\n", s);
    
    // read the data back
    char data_read[writeDataLen];
    readEEPROM(address, pointerAdddress+add*64, data_read, writeDataLen);
    pc.printf("Data read: %s\n", data_read);
		add++;
		if (add == 5){
			for (int i =0; i< 10; i++){
				readEEPROM(address, i*64, data_read, 64);
				pc.printf("Previous data stored: %s\n", data_read);
				Thread::wait(250);
			}
			add=0;
		}
    }
 
}