#include "mbed.h"

#ifndef SISTEMA_H

#define SISTEMA_H

#define ST 0 //Set time
#define GT 1 //Get time
#define SC 2 //Set Cadency
#define GC 3 //Get Cadency
#define GD 4 //Get Data
#define LD 5 //Lod Del
#define SS 6 //Set Sampling Freq

void funct_th_sistema(void);

//extern char *rx_sist[21];//no deberia ser externo



#endif