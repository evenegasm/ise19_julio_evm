#include "mbed.h"
#include "rtos.h"

#ifndef GLOBALES_H
#define GLOBALES_H

#define ACK 	0x06
#define NACK 	0x21
#define STX 	0x02
#define ETX 	0x03

#define EVENTO_RX_CR  	1
#define EVENTO_TX_NACK	2
#define EVENTO_TX_ACK		4
#define EVENTO_RX_TRAMA	8
#define EVENTO_REENVIO  16
#define EVENTO_TX_TRAMA	32
#define EVENTO_TX_INT	64

#define TIMEOUT 				3000
#define BAUD_RATE 			38400

#define TIEMPO 0
#define DATOS 1


extern RawSerial serial;

extern Thread id_th_rx;
extern Thread id_th_tx;
extern Thread id_th_sistema;
extern Thread id_th_time;
extern Thread id_th_muestreo;

extern time_t seconds;

extern char bcc;
extern int fs;
extern int cadencia;

extern Queue<char, 50> colaBuffer;
extern Queue<char, 50> colaTx;


typedef unsigned int estados;

int toSeconds(char dia0, char dia1, char mes0, char mes1, char ano0, char ano1, char horas1, char horas0, char min1, char min0, char seg1, char seg0);

#endif