//Medir la frecuencia de una se�al cuadrada entre el pin 30 y masa del lpc1768 entre 100 Hz y 10 kHz sin error
#include "mbed.h"

DigitalIn clkin(p30);   // for RESERVE pin30 as CAP2[0]
Timeout t;
Thread id_th_medida(osPriorityNormal,1024);
Serial pc(USBTX, USBRX);
int data;
// Reset Counter and Count Start
void P30_RESET_CTR(void)
{
    LPC_TIM2->TCR = 2;             // Reset the counter (bit1<=1,bit0<=0)
    LPC_TIM2->TCR = 1;             // UnReset counter (bit1<=0,bit0<=1)
}
 
// Get Counter Value
int P30_GET_CTR(void)
{
    return LPC_TIM2->TC; // Read the counter value
}
 
// Setting p30 to Cap2.0
void P30_INIT_CTR(void)
{
    LPC_SC->PCONP |= 1 << 22;               // 1)Power up TimerCounter2 (bit22)
    LPC_PINCON->PINSEL0 |= 3 << 8;          // 2)Set P0[4] to CAP2[0]
    LPC_TIM2->TCR = 2;                          // 3)Counter Reset (bit1<=1,bit0<=0)
    LPC_TIM2->CTCR = 1;                     // 4)Count on riging edge Cap2[0]
    LPC_TIM2->CCR = 0;                                          // 5)Input Capture Disabled
    LPC_TIM2->TCR = 1;                          // 6)Counter Start (bit1<=0,bit0<=1)
}

void handler() {
		data=P30_GET_CTR();
		P30_RESET_CTR();
		id_th_medida.signal_set(0x1);
}

void func_th_medida(void){
	while(1){
		P30_INIT_CTR();
		t.attach(&handler, 1.0);
		//Thread::wait(1500);
		id_th_medida.signal_wait(0x1,osWaitForever);
		pc.printf("pin30 Freq = %d (Hz)\r\n",data);
		
			
	}
}
int main() {	
	pc.printf("Lanzo hilo\r\n");
	id_th_medida.start(func_th_medida);
	Thread::wait(osWaitForever);
}