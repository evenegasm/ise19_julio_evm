#include "sistema.h"
#include "globales.h"

//Constantes
char  SetTimeConst[8] = { 'S','e','t','T','i','m','e',' ' };
char  GetTimeConst[7] = { 'G','e','t','T','i','m','e' };
char  SetCadencyConst[11] = { 'S','e','t','C','a','d','e','n','c','y',' ' };
char  GetCadencyConst[11] = { 'G','e','t','C','a','d','e','n','c','y',' ' };
char  GetDataConst[8] = { 'G','e','t','D','a','t','a',' ' };
char  LogDelConst[6] = { 'L','o','g','D','e','l'};
char  SetSamplingConst[16] = { 'S','e','t','S','a','m','p','l','i','n','g','F','r','e','q','=' };

char  GetCadencySend[8] = { 'C','a','d','e','n','c','y',' '};

estados estado;
char comando [256];
osEvent evento;
char *rx_sist[21];
char sist_tx[49];
int tamanoTrama;
int i;
char buf_time2[32];
char horaSet[8];
//char aux[35];

void enviarTrama(char buff[]){
	while(!colaTx.empty()){		//Se vacia la cola
		colaTx.get();
	}

	for(i=0; i< sizeof(sist_tx); i++) {
		colaTx.put(&buff[i]);		//Se llena la cola con los punteros de los caracteres a enviar
		bcc ^= sist_tx[i];
	}
	colaTx.put(NULL);
	bcc |= 0x20;
}

bool cmp_trama(char cmp1[], char cmp2[], int size){  //Revisar
	bool fCmp = true;
	int i = 0;
	for(i=0;i<size;i++){
		if(cmp1[i]!=cmp2[i]){
			fCmp = false;
		}
	}
	return fCmp;
}


void funct_th_sistema(void){
		while(1){
		evento = id_th_rx.signal_wait(0); //Se esperan se�ales de enviar NACK(2), enviar ACK(4) o enviar trama(8)
		bcc = 0;
		switch(evento.value.signals) {
			case EVENTO_TX_NACK: //NACK
				colaTx.put((char*)NACK);
				colaTx.put(NULL);
				id_th_tx.signal_set(EVENTO_TX_NACK);
				break;
			case EVENTO_TX_ACK: //ACK
				colaTx.put((char*)ACK);
				colaTx.put(NULL);
				id_th_tx.signal_set(EVENTO_TX_ACK);
				break;
			case EVENTO_RX_TRAMA: //TRAMA 
				tamanoTrama = 0;
				for(i=0;i<sizeof(sist_tx);i++){ 	//Se vacia el buffer de datos
					sist_tx[i] = 0;
				}
				do {
					rx_sist[tamanoTrama]=(char*)colaBuffer.get().value.p;
					tamanoTrama++;
				}while(*rx_sist[tamanoTrama-1] != NULL);		// Se llena el buffer con los datos de la cola
				tamanoTrama--;
				
				if		 (cmp_trama(SetTimeConst, 		*rx_sist, 8))  { estado = ST; }  //La funcion cmp_trama utiliza el tama�o del primer par�metro
				else if(cmp_trama(GetTimeConst, 		*rx_sist, 7))  { estado = GT; }
				else if(cmp_trama(SetCadencyConst, 	*rx_sist, 11)) { estado = SC; }
				else if(cmp_trama(GetCadencyConst, 	*rx_sist, 11)) { estado = GC; }
				else if(cmp_trama(GetDataConst, 		*rx_sist, 8))  { estado = GD; }
				else if(cmp_trama(LogDelConst, 			*rx_sist, 6))  { estado = LD; }
				else if(cmp_trama(SetSamplingConst, *rx_sist, 16)) { estado = SS; }
				
				switch(estado) {	//Se clasifican por el tama�o de trama recibida
					case ST: //Poner en hora
						//									char dia1, char dia0,      char mes1,    char mes0,    char ano1,     char ano0,  char horas1, char horas0,   char min1,    char min0,      char seg1,  char seg0
						set_time(toSeconds(*rx_sist[8], *rx_sist[9], *rx_sist[11], *rx_sist[12], *rx_sist[14], *rx_sist[15], *rx_sist[17], *rx_sist[18], *rx_sist[20], *rx_sist[21], *rx_sist[23], *rx_sist[24]));
						break;//*************TIMETOSECONDS FUNCIONA????�?�?
					case GT: //Enviar hora
						strftime(buf_time2, 32, "%d/%m/%y %H:%M:%S %w", localtime(&seconds));//******Formato correcto? %w=[0(Sunday),6]? 32 bits?
						memcpy(sist_tx, buf_time2, sizeof(buf_time2)*sizeof(char));
						enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
						break;
					case SC: //Modificar cadencia de datos almacenados
						if((int)*rx_sist[11] > 0)
							cadencia = (int)*rx_sist[11];
						else
							cadencia = 1;
						break;
					case GC: //Obtiene cadencia de datos almacenados
						memcpy(sist_tx, GetCadencySend, sizeof(GetCadencySend)*sizeof(char));
						strcat(sist_tx, (const char*)cadencia);//*******************************OK?
					  enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
						break;
					case GD: //Obtiene un numero de datos almacenados
						id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
						break;
					case LD: //Elimina datos
						break;
					case SS: //Configura la frecuencia de muestreo
						break;
				}
					
				break;
			case EVENTO_REENVIO: //NACK RECIVIDO. Se reenvia la ultima trama enviada hasta 3 veces.
						bcc = 0;
						enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);
				break;
		
		}
	}
}
