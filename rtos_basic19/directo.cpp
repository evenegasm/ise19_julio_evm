#include "directo.h"

DigitalIn clkin(p30);   // for RESERVE pin30 as CAP2[0]
Timeout tout;

// Reset Counter and Count Start
void P30_RESET_CTR(void)
{
    LPC_TIM2->TCR = 2;             // Reset the counter (bit1<=1,bit0<=0)
    LPC_TIM2->TCR = 1;             // UnReset counter (bit1<=0,bit0<=1)
}
 
// Get Counter Value
int P30_GET_CTR(void)
{
    return LPC_TIM2->TC; // Read the counter value
}
 
// Setting p30 to Cap2.0
void P30_INIT_CTR(void)
{
    LPC_SC->PCONP |= 1 << 22;               // 1)Power up TimerCounter2 (bit22)
    LPC_PINCON->PINSEL0 |= 3 << 8;          // 2)Set P0[4] to CAP2[0]
    LPC_TIM2->TCR = 2;                          // 3)Counter Reset (bit1<=1,bit0<=0)
    LPC_TIM2->CTCR = 1;                     // 4)Count on riging edge Cap2[0]
    LPC_TIM2->CCR = 0;                                          // 5)Input Capture Disabled
    LPC_TIM2->TCR = 1;                          // 6)Counter Start (bit1<=0,bit0<=1)
}

//Handler de la interrupci�n
void handler() {
		fd = P30_GET_CTR();
		P30_RESET_CTR();
		id_th_directo.signal_set(EVENTO_DESPERTAR);
		
}

//Funci�n del hilo
void funct_th_directo() {
	while(1){
		P30_INIT_CTR();
		tout.attach(&handler, 1.0);//Timeout
		id_th_directo.signal_wait(EVENTO_DESPERTAR);	
	}
}