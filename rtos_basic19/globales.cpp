#include "globales.h"

RawSerial serial(USBTX, USBRX, BAUD_RATE);

//Hilos
Thread id_th_rx(osPriorityNormal,1024); 				//hilo receptor
Thread id_th_tx(osPriorityNormal,1024); 				//hilo transmisor
Thread id_th_sistema(osPriorityNormal,2048);  	//hilo de control de tramas
Thread id_th_time(osPriorityNormal,1024); 			//hilo de tiempo
Thread id_th_muestreo(osPriorityNormal,2048); 	//hilo funcionalidad
Thread id_th_directo(osPriorityNormal,1024); 	//hilo cadena directa


float fs = 1.0; // frecuencia de muestreo, milisegundos a esperar
int cadencia = 1; // periodo de almacenamiento de datos

int actual = 0;

int fd = 0; //Frecuencia medida por la cadena directa
//Estructura de tiempo
time_t seconds;

char bcc;

Queue<char, 200> colaBuffer;
Queue<char, 200> colaTx;

Mutex cerrojo;

int toSeconds(char dia1, char dia0, char mes1, char mes0, char ano1, char ano0, char horas1, char horas0, char min1, char min0, char seg1, char seg0){
	return ((1036800+31536000*((10*((int)ano1-48)+(int)ano0-48)+30))+(2592000*(10*((int)mes1-48)+(int)mes0-49))+(86400*(10*((int)dia1-48)+(int)dia0-48))+(3600*(10*((int)horas1-48)+(int)horas0-48))+(60*(10*((int)min1-48)+(int)min0-48))+(10*((int)seg1-48)+(int)seg0-48));
}//La primera suma son los dias extra por a�os visiestos; en mes se resta 1 estra porque el mes en el que ponemos al fecha a�n no ha pasado

//SDA SCL
I2C i2c(p9, p10);
int address = 0xA0;
int pointerAdddress = 0;//Direccion inicial

bool envioDatos =false;
bool ocupado = false;

void writeEEPROM(int address, unsigned int eeaddress, char *data, int size){
		cerrojo.lock();
		ocupado = true;
    char i2cBuffer[size + 2];
    i2cBuffer[0] = (unsigned char)(eeaddress >> 8); // MSB
    i2cBuffer[1] = (unsigned char)(eeaddress & 0xFF); // LSB
 
    for (int i = 0; i < size; i++) {
        i2cBuffer[i + 2] = data[i];
    }
 
    int result = i2c.write(address, i2cBuffer, size + 2, false);
    Thread::wait(6);
		ocupado = false;
		cerrojo.unlock();
}
 
// this function has no read limit
void readEEPROM(int address, unsigned int eeaddress, char *data, int size){
		cerrojo.lock();
		ocupado = true;
    char i2cBuffer[2];
    i2cBuffer[0] = (unsigned char)(eeaddress >> 8); // MSB
    i2cBuffer[1] = (unsigned char)(eeaddress & 0xFF); // LSB
 
    // Reset eeprom pointer address
    int result = i2c.write(address, i2cBuffer, 2, false);
    Thread::wait(6);
 
    // Read eeprom
    i2c.read(address, data, size);
		Thread::wait(6);
		ocupado = false;
		cerrojo.unlock();
}