#include "mbed.h"
#include "rtos.h"

#ifndef GLOBALES_H
#define GLOBALES_H

#define ACK 	0x06
#define NACK 	0x21
#define STX 	0x02
#define ETX 	0x03

#define EVENTO_RX_CR  		0x1
#define EVENTO_TX_NACK		0x2
#define EVENTO_TX_ACK			0x4
#define EVENTO_RX_TRAMA		0x8
#define EVENTO_REENVIO  	0x10
#define EVENTO_TX_TRAMA		0x20
#define EVENTO_STOP				0x40
#define EVENTO_DESPERTAR	0x80
#define EVENTO_DATA				0x100
#define EVENTO_NO_ENVIO		0x200

#define TIMEOUT 				3000
#define BAUD_RATE 			38400

#define TIEMPO 0
#define DATOS 1
#define TAMANO_DATOS 64//Revisar

extern RawSerial serial;

extern Thread id_th_rx;
extern Thread id_th_tx;
extern Thread id_th_sistema;
extern Thread id_th_time;
extern Thread id_th_muestreo;
extern Thread id_th_directo;

extern time_t seconds;

extern char bcc;
extern float fs;
extern int cadencia;
extern int actual;

extern int fd;

extern Queue<char, 200> colaBuffer;
extern Queue<char, 200> colaTx;


typedef unsigned int estados;

int toSeconds(char dia0, char dia1, char mes0, char mes1, char ano0, char ano1, char horas1, char horas0, char min1, char min0, char seg1, char seg0);

extern bool envioDatos;
extern bool ocupado;

extern I2C i2c;
extern int address;
extern int pointerAdddress;//Direccion inicial

void writeEEPROM(int address, unsigned int eeaddress, char *data, int size);
void readEEPROM(int address, unsigned int eeaddress, char *data, int size);

#endif