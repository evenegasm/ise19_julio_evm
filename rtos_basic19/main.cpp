#include "mbed.h"
#include "globales.h"
#include "receptor.h"
#include "transmisor.h"
#include "sistema.h"
#include "time_upm.h"
#include "muestreo.h"
#include "directo.h"

 


int main() {
		//Inicializacion del reloj   

		set_time(0);
		
	  //Definimos la comunicacion segun las especificaciones
		serial.format(8, SerialBase::Even, 1);
	
		//Inicializacion de los hilos
    id_th_rx.start(funct_th_rx);
		id_th_tx.start(funct_th_tx);
		id_th_sistema.start(funct_th_sistema);
    id_th_time.start(funct_th_time);
		id_th_muestreo.start(funct_th_muestreo);
	  id_th_directo.start(funct_th_directo);
	
		Thread::wait(osWaitForever);
}
