#include "muestreo.h"
#include "stdio.h"

#define ADC_SCALE_VALUE 3.3/4096
#define MCP3201_CLK_FREQ    1000000
// MCP3201 SPI channel pinout
// p5 not wired, there are no commands to send for MCP3201 device
SPI MCP3201(p5,p6,p7);
// take control from CS pin to avoid transition changes between write commands
DigitalOut cs(p21); 
char aux[TAMANO_DATOS];
char hora [32];
char s [10];

	
Timeout toutFs;
Timeout toutCad;


int ReadAnalogMCP3201(void){
 //uncommente below line only if the MCP3201_CLK_FREQ is diferrent of 1MHz
 //MCP3201.frequency(MCP3201_CLK_FREQ);
 
 cs=0;
 int high_byte = MCP3201.write(0x00);
 int low_byte = MCP3201.write(0x00);
 cs=1;
 int conv_result = ((high_byte & 0x1f) << 7) | ((low_byte >> 1) & 0x7f);  
 
 return conv_result;
}

void muestrear(){
	id_th_muestreo.signal_set(EVENTO_DATA);
}
void escribir(){
	id_th_muestreo.signal_set(EVENTO_NO_ENVIO);
}

void funct_th_muestreo() {
	osEvent evento;
	toutFs.attach(&muestrear,fs);	
	Thread::wait(50);
	toutCad.attach(&escribir,(float)cadencia);
	float fi;
	float vi;
	
	while(1){
		evento = Thread::signal_wait(0);	//Se espera a que llegue una se�al o a que haya que guardar en memoria
		switch (evento.value.signals){
			case EVENTO_STOP:
				Thread::signal_wait(EVENTO_DESPERTAR);//Espera a que se terminen de enviar los datos
				break;
			case EVENTO_DATA: //Medir frecuencia del sistema
				vi=(float)ReadAnalogMCP3201() * ADC_SCALE_VALUE; // show value in volts.
				fi =(vi *43000)/(2.09*100000*6800*0.00000001);
				toutFs.attach(&muestrear,fs);
				break;
			case EVENTO_NO_ENVIO: //Guardar datos en la EEprom
					strcpy(aux, "R");
					sprintf(s, "%d", actual);
					strcat(aux, s);
					strcat(aux, " [");
					strftime(hora, 17, "%d/%m/%y %H:%M:%S", localtime(&seconds)); //"Ryyy [DD/MM/AAAA hh:mm:ss] Fd:aaaaa Fi:bbbbb (c,cc V)"
					strcat(aux, hora);
					strcat(aux, "] Fd:");
					sprintf(s, "%d", fd);
					strcat(aux, s);
					strcat(aux, " Fi:");
					sprintf(s, "%d", (int)fi);
					strcat(aux, s);
					strcat(aux, " (");
					sprintf(s, "%4.2f", vi);
					strcat(aux, s);
					strcat(aux, " V)");
					if (!ocupado){
						writeEEPROM(address, pointerAdddress, aux, TAMANO_DATOS);
						pointerAdddress = pointerAdddress+ TAMANO_DATOS;
						actual=pointerAdddress/TAMANO_DATOS;
					}
					toutCad.attach(&escribir,(float)cadencia);
				break;
		}
				
	}

}

