#include "mbed.h"
#include "rtos.h"
#include "globales.h"

#ifndef MUESTREO_H

#define MUESTREO_H

#define MUESTREO 0
#define GUARDAR  1
#define FS    	 2 
#define CADENCIA 3 
#define SACAR    4 
#define BORRAR   5 

void funct_th_muestreo(void);


#endif