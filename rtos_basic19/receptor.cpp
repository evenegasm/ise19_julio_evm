#include "receptor.h"
#include "globales.h"
int m, n = 0; 

char BufferIN[20];
char CR;
unsigned int indWbrx;
int cnt;
bool fin = true;

void isr_rx(){
			CR= (char)serial.getc();
			id_th_rx.signal_set(EVENTO_RX_CR);
}

void funct_th_rx(void) {					//Aut�mata de recepci�n

estados estado = REPOSO;		//Estado del aut�mata
osEvent evento;							//Informaci�n de evento retornadopor funciones RTX
char bcc;				//Byte para almacenar BCC
serial.attach(isr_rx,Serial::RxIrq);

	while(true){							//Bucle de la tarea
			
			switch (estado) {		//Determinar estado actual
				
				case REPOSO:
					
					evento = id_th_rx.signal_wait(EVENTO_RX_CR);	//Espera a que se reciba un caracter. El evento se genera en la interrupci�n de Rx
				
					if (CR == STX){							//Comprobar si es <STX> iniico de trama
						estado = RECIBE;					//Se transita de estado
						bcc = 0;									//Se inicializa el c�lculo del BCC
						indWbrx = 0;							//Se resetea el indice de datos recibido
						while(!colaBuffer.empty()){		//Se vacia la cola
							colaBuffer.get();
						}
					}
				
				break;
				
				case RECIBE:
					
					evento = id_th_rx.signal_wait(EVENTO_RX_CR,TIMEOUT);	//Se espera a que se reciba un caracter o se produzca un timeout
				
					if (evento.value.signals & EVENTO_RX_CR){		//se ha recibido un caracter
						
						switch (CR) {
							case ETX:								//Si es <ETX>, se finaliza la trama 
								estado = BCC;					//Se transita a estado de espera BCC 
								bcc = bcc | 0x20;			//Se hace que el caracter sea imprimible
							break;
							case STX:								//Si es <STX> se aborta la trama en curso y se recibe una trama nueva
								bcc = 0;
								indWbrx = 0;
								cnt = 0;
							break;
							default:								
								bcc = bcc ^ CR;						//se va calculando el BCC
								BufferIN[indWbrx++] = CR;	//Se almacena el caracter recibido
								colaBuffer.put(&BufferIN[indWbrx-1]);
							break;
						}
					}else{
						estado = REPOSO;											//Se ha producido timeout y se transita a reposo
						id_th_sistema.signal_set(EVENTO_TX_NACK);	//Manda evento enviar NACK al Automata Tx
					}
				
				break;
				
				case BCC:								//Se calcula el BCC
					
					evento = id_th_rx.signal_wait(EVENTO_RX_CR, TIMEOUT);	//Se espera a que se reciba un caracter o se produzca un timeout
				
					if (evento.value.signals & EVENTO_RX_CR){					//Se ha recibido un caracter
						if (CR == bcc){		//Si BCC correcto
							BufferIN[indWbrx++] = NULL;		//Cerrar trma con NULL
							colaBuffer.put(NULL);
							indWbrx = 0;
							id_th_sistema.signal_set(EVENTO_TX_ACK);	//Manda evento enviar ACK al Automata Tx
							id_th_sistema.signal_set(EVENTO_RX_TRAMA);//Se manda evento para proicesar trama
							estado = ACK;
														
						}
						else{																		//Mal bcc
							id_th_sistema.signal_set(EVENTO_TX_NACK);	//Manda evento enviar NACK al Automata Tx
							estado = REPOSO;
						}
					}
					else {																		//Se ha producido timeout
						id_th_sistema.signal_set(EVENTO_TX_NACK);		//Manda evento enviar NACK al Automata Tx
						estado = REPOSO;
					}
					
				break;	
				case ACK:
					evento = id_th_rx.signal_wait(0, TIMEOUT);//EVENTO_RX_CR
					if (envioDatos){//Envio de los datos de la EEprom
						if(((evento.value.signals & EVENTO_RX_CR) && CR == ACK) ){//Llega ACK
							id_th_sistema.signal_set(EVENTO_DATA);
						}else if (cnt < 2 || ((evento.value.signals & EVENTO_RX_CR ) && (CR == NACK))){//Llega NACK o TIMEOUT
							id_th_sistema.signal_set(EVENTO_REENVIO);
							cnt++;
						}else if ((cnt==2) || (evento.value.signals & EVENTO_DESPERTAR)){//Alcanzo max reenvios o Se han acabado los datos a transmitir
							if((cnt==2))
								id_th_sistema.signal_set(EVENTO_NO_ENVIO);
							estado = REPOSO;
							cnt = 0;
							envioDatos = false;
						}
					}else{//Funcionamiento normal
						if(((evento.value.signals & EVENTO_RX_CR) && CR == ACK) || cnt==2 || (evento.value.signals !=0xBB8)) {//Si se recibe ack o se agota el contador o no hay que enviar respuesta
							estado = REPOSO;
							cnt = 0;
						}else if (cnt < 2 || ((evento.value.signals & EVENTO_RX_CR ) && (CR == NACK)) ){//Si timeout o nack
							id_th_sistema.signal_set(EVENTO_REENVIO);
							cnt++;
						}
					}
					break;
			}
		}
}