#include "sistema.h"
#include "globales.h"

//Constantes
char  SetTimeConst[8] = { 'S','e','t','T','i','m','e',' ' };
char  GetTimeConst[7] = { 'G','e','t','T','i','m','e' };
char  SetCadencyConst[11] = { 'S','e','t','C','a','d','e','n','c','y',' ' };
char  GetCadencyConst[10] = { 'G','e','t','C','a','d','e','n','c','y'};
char  GetDataConst[8] = { 'G','e','t','D','a','t','a',' ' };
char  LogDelConst[6] = { 'L','o','g','D','e','l'};
char  SetSamplingConst[16] = { 'S','e','t','S','a','m','p','l','i','n','g','F','r','e','q','=' };

char  GetCadencySend[8] = { 'C','a','d','e','n','c','y',' '};

estados estado;
char comando [256];
osEvent evento;
char *rx_sist[30];
char sist_tx[TAMANO_DATOS];
int tamanoTrama;
char buf_time2[32];
char horaSet[8];
char borrar[TAMANO_DATOS];

char cad [1];

int numDatos = 0;
char dato[TAMANO_DATOS];


void enviarTrama(char buff[]){
	while(!colaTx.empty()){		//Se vacia la cola
		colaTx.get();
	}

	for(int i=0; i< sizeof(sist_tx); i++) {
		colaTx.put(&buff[i]);		//Se llena la cola con los punteros de los caracteres a enviar
		bcc ^= sist_tx[i];
	}
	colaTx.put(NULL);
	bcc |= 0x20;
}

bool cmp_trama(char cmp1[], char cmp2[], int size){  //Revisar
	bool fCmp = true;
	for(int i=0;i<size;i++){
		if(cmp1[i]!=cmp2[i]){
			fCmp = false;
		}
	}
	return fCmp;
}


void funct_th_sistema(void){
	int iteraciones =0;
	strcpy(borrar, " ");
	for (int i=0; i< TAMANO_DATOS-1; i++){
		strcat(borrar, " ");
	}
		while(1){
		evento = id_th_sistema.signal_wait(0); //Se esperan se�ales de enviar NACK(2), enviar ACK(4) o enviar trama(8)
		bcc = 0;
		switch(evento.value.signals) {
			case EVENTO_TX_NACK: //NACK
				colaTx.put((char*)NACK);
				colaTx.put(NULL);
				id_th_tx.signal_set(EVENTO_TX_NACK);
				break;
			case EVENTO_TX_ACK: //ACK
				colaTx.put((char*)ACK);
				colaTx.put(NULL);
				id_th_tx.signal_set(EVENTO_TX_ACK);
				break;
			case EVENTO_RX_TRAMA: //TRAMA 
				tamanoTrama = 0;
				for(int i=0;i<sizeof(sist_tx);i++){ 	//Se vacia el buffer de datos
					sist_tx[i] = 0;
				}
				do {
					rx_sist[tamanoTrama]=(char*)colaBuffer.get().value.p;
					tamanoTrama++;
				}while(*rx_sist[tamanoTrama-1] != NULL);		// Se llena el buffer con los datos de la cola
				tamanoTrama--;
				
				if		 (cmp_trama(SetTimeConst, 		*rx_sist, 8))  { id_th_rx.signal_set(EVENTO_NO_ENVIO); estado = ST; }  //La funcion cmp_trama utiliza el tama�o del primer par�metro
				else if(cmp_trama(GetTimeConst, 		*rx_sist, 7))  { estado = GT; }
				else if(cmp_trama(SetCadencyConst, 	*rx_sist, 11)) { id_th_rx.signal_set(EVENTO_NO_ENVIO); estado = SC; }
				else if(cmp_trama(GetCadencyConst, 	*rx_sist, 10)) { estado = GC; }
				else if(cmp_trama(GetDataConst, 		*rx_sist, 8))  { estado = GD; }
				else if(cmp_trama(LogDelConst, 			*rx_sist, 6))  { id_th_rx.signal_set(EVENTO_NO_ENVIO); estado = LD; }
				else if(cmp_trama(SetSamplingConst, *rx_sist, 16)) { id_th_rx.signal_set(EVENTO_NO_ENVIO); estado = SS; }
				
				switch(estado) {	//Se clasifican por el tama�o de trama recibida
					case ST: //Poner en hora
						//									char dia1, char dia0,      char mes1,    char mes0,    char ano1,     char ano0,  char horas1, char horas0,   char min1,    char min0,      char seg1,  char seg0
						set_time(toSeconds(*rx_sist[8], *rx_sist[9], *rx_sist[11], *rx_sist[12], *rx_sist[14], *rx_sist[15], *rx_sist[17], *rx_sist[18], *rx_sist[20], *rx_sist[21], *rx_sist[23], *rx_sist[24]));
						break;
					case GT: //Enviar hora
						strftime(buf_time2, 32, "%d/%m/%y %H:%M:%S %w", localtime(&seconds));
						memcpy(sist_tx, buf_time2, sizeof(buf_time2)*sizeof(char));
						enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
						break;
					case SC: //Modificar cadencia de datos almacenados
						if((int)*rx_sist[11]-48 > 0)
							cadencia = (int)*rx_sist[11]-48;//-48 para pasar eliminar el offset por ascii
						else
							cadencia = 1;
						break;
					case GC: //Obtiene cadencia de datos almacenados
						memcpy(sist_tx, GetCadencySend, sizeof(GetCadencySend)*sizeof(char));
						sprintf(cad, "%d", cadencia);
						strcat(sist_tx, cad);
					  enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
						break;
					case GD: //Obtiene un numero de datos almacenados
						bool bucle = true;
						envioDatos = true;
						switch (tamanoTrama){//Obtiene el n�mero de datos a devolver
							case 9:
								numDatos = (int)*rx_sist[8]-48;
								break;
							case 10:
								if (*rx_sist[8] == '-' && *rx_sist[9] == '1')
									numDatos = pointerAdddress/TAMANO_DATOS;
								else
									numDatos = ((int)*rx_sist[8]-48)*10 + ((int)*rx_sist[9]-48);
								break;
							case 11:
								numDatos = ((int)*rx_sist[8]-48)*100 + ((int)*rx_sist[9]-48)*10 + ((int)*rx_sist[10]-48);
								break;
						}
						int j =0;
						while(j<numDatos){
							bucle = true;
							//lee dato de la memoria
							readEEPROM(address, j*TAMANO_DATOS, dato, TAMANO_DATOS);
							//mete dato en cola
							memcpy(sist_tx, dato, sizeof(dato)*sizeof(char));
							enviarTrama(sist_tx);
							id_th_tx.signal_set(EVENTO_TX_TRAMA);//Envia respuesta
							while(bucle){
								evento=Thread::signal_wait(0);
								switch(evento.value.signals){
									case EVENTO_REENVIO:
										bcc = 0;
										enviarTrama(sist_tx);
										id_th_tx.signal_set(EVENTO_TX_TRAMA);
									break;
									case EVENTO_DATA:
										bucle = false;
									break;
									case EVENTO_NO_ENVIO:
										j = numDatos;
										bucle = false;
									break;
								}
							}
							j++;
						}
						envioDatos = false;
						id_th_rx.signal_set(EVENTO_DESPERTAR);
						break;
					case LD: //Elimina datos
						//Borrar datos
						for (int h=0; h< pointerAdddress; h=h+TAMANO_DATOS){
							writeEEPROM(address, h, borrar, TAMANO_DATOS);
						}
						actual=0;
						pointerAdddress = 0;
						break;
					case SS: //Configura la frecuencia de muestreo
						int datos = 1;
						switch (tamanoTrama){//Obtiene el n�mero de datos por segundo
							case 17:
								datos = (int)*rx_sist[16]-48;
								break;
							case 18:
								datos = ((int)*rx_sist[16]-48)*10 + ((int)*rx_sist[17]-48);
								break;
							case 19:
								datos = ((int)*rx_sist[16]-48)*100 + ((int)*rx_sist[17]-48)*10 + ((int)*rx_sist[18]-48);
								break;
							case 20:
								datos = ((int)*rx_sist[16]-48)*1000 + ((int)*rx_sist[17]-48)*100 + ((int)*rx_sist[18]-48)*10 + ((int)*rx_sist[19]-48);
								break;
						}
						fs=1.0/datos;//Segundos entre muestras
						break;
				}
					
				break;
			case EVENTO_REENVIO: //NACK RECIVIDO. Se reenvia la ultima trama enviada hasta 3 veces.
						bcc = 0;
						enviarTrama(sist_tx);
						id_th_tx.signal_set(EVENTO_TX_TRAMA);
				break;
		}
	}
}
